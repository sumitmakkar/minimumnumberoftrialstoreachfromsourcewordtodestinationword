#include <iostream>
#include <vector>

#define MAX_CHILDREN 100

using namespace std;

typedef struct node_structure
{
    string                name;
    int                   children;
    struct node_structure *childrenNodes[MAX_CHILDREN];
    int                   childCost[MAX_CHILDREN];
    int                   cost;
    int                   positionInList;
    bool                  isVisited;
}node_struct;

typedef struct queue_structure
{
    node_struct            data;
    struct queue_structure *next;
}queue_struct;

class Graph
{
    private:
        vector<string> dictionaryVector;
        
        node_struct* createNewNode(string str , int pos)
        {
            node_struct *newNode = new node_struct();
            if(newNode)
            {
                newNode->name             = str;
                newNode->children         = 0;
                newNode->childrenNodes[0] = NULL;
                newNode->childCost[0]     = 1;
                newNode->cost             = 1000;
                newNode->positionInList   = pos;
                newNode->isVisited        = false;
            }
            return newNode;
        }
        
        string createStringWithSpace(string str , int loc)
        {
            string newStrWithSpace = "";
            int    len             = (int)str.length();
            int    j               = -1;
            for(int i = 0 ; i <= len ; i++)
            {
                if(i == loc)
                {
                    newStrWithSpace += " ";
                }
                else
                {
                    newStrWithSpace += str[++j];
                }
            }
            return newStrWithSpace;
        }
        
        bool stringComparisionResult(string str1 , string str2)
        {
            int len       = (int)str1.length();
            int diffCount = 0;
            for(int i = 0 ; i < len ; i++)
            {
                diffCount += str1[i] == str2[i] ? 0 : 1;
                if(diffCount > 1)
                {
                    return false;
                }
            }
            return true;
        }
        
        bool compareStrings(string str1 , string str2)
        {
            string smallerStr = str1.length() < str2.length() ? str1 : str2;
            string largerStr  = !(smallerStr.compare(str1))   ? str2 : str1;
            bool   result     = false;
            if(abs((int)smallerStr.length() - (int)largerStr.length()) > 1)
            {
                return false;
            }
            if((int)str1.length() == (int)str2.length())
            {
                result |= stringComparisionResult(str1 , str2);
            }
            else
            {
                int len = (int)largerStr.length();
                for(int i = 0 ; i < len ; i++)
                {
                    string strForComp  = createStringWithSpace(smallerStr , i);
                    result            |= stringComparisionResult(largerStr , strForComp);
                    if(result)
                    {
                        break;
                    }
                }
            }
            return result;
        }
        
    public:
        Graph(vector<string> dVector)
        {
            dictionaryVector = dVector;
        }
        
        vector<node_struct> createNodesVector()
        {
            vector<node_struct> finalVector;
            int len = (int)dictionaryVector.size();
            for(int i = 0 ; i < len ; i++)
            {
                finalVector.push_back(*createNewNode(dictionaryVector[i] , i));
            }
            len = (int)finalVector.size();
            for(int i = 0 ; i < len-1 ; i++)
            {
                for(int j = i+1 ; j < len ; j++)
                {
                    if(compareStrings(finalVector[i].name , finalVector[j].name))
                    {
                        finalVector[i].childrenNodes[finalVector[i].children] = &finalVector[j];
                        finalVector[i].childCost[finalVector[i].children]     = 1;
                        finalVector[i].children++;
                        finalVector[j].childrenNodes[finalVector[j].children] = &finalVector[i];
                        finalVector[j].childCost[finalVector[j].children]     = 1;
                        
                        finalVector[j].children++;
                    }
                }
            }
            return finalVector;
        }
        
        void display(vector<node_struct> graphNode)
        {
            int len = (int)graphNode.size();
            for(int i = 0 ; i < len ; i++)
            {
                cout<<graphNode[i].positionInList<<". Parent : "<<graphNode[i].name<<endl<<"Children"<<endl;
                for(int j = 0 ; j < graphNode[i].children ; j++)
                {
                    cout<<((node_struct *)(graphNode[i].childrenNodes[j]))->positionInList<<". "<<((node_struct *)(graphNode[i].childrenNodes[j]))->name<<endl;
                }
                cout<<endl;
            }
        }
};

class MyQueue
{
    private:
        queue_struct *queueHead;
        queue_struct *queueTail;
        queue_struct* createNewNode(node_struct node)
        {
            queue_struct *newNode = new queue_struct();
            if(newNode)
            {
                newNode->data = node;
                newNode->next = NULL;
            }
            return newNode;
        }
    
    public:
        MyQueue()
        {
            queueHead = NULL;
            queueTail = NULL;
        }
    
        bool checkIfQueueIsNotEmpty()
        {
            return queueHead ? true : false;
        }
    
        void queuePush(node_struct *vectorNode)
        {
            queue_struct *newNode = createNewNode(*vectorNode);
            if(!queueHead)
            {
                queueHead = newNode;
                queueTail = newNode;
            }
            else
            {
                queueTail->next = newNode;
                queueTail       = queueTail->next;
            }
        }
    
        queue_struct* queuePop()
        {
            if(checkIfQueueIsNotEmpty())
            {
                queue_struct *nodeToBeReturned = queueHead;
                queue_struct *nodeToBeDeleted  = queueHead;
                queueHead                      = queueHead->next;
                free(nodeToBeDeleted);
                nodeToBeDeleted                = NULL;
                return nodeToBeReturned;
            }
            return NULL;
        }
};

class Engine
{
    private:
        vector<node_struct> graphVector;
        int                 sourceLoc;
        int                 destinationLoc;
        MyQueue             bfsQueue;
    
        void getSourceAndDestinationLocationsInVector(string source , string destination)
        {
            int len = (int) graphVector.size();
            for(int i = 0 ; i < len ; i++)
            {
                if(!source.compare(graphVector[i].name))
                {
                    sourceLoc = i;
                }
                if(!destination.compare(graphVector[i].name))
                {
                    destinationLoc = i;
                }
            }
        }
        
    public:
        Engine(vector<node_struct> gVec , string sStr , string dStr)
        {
            graphVector = gVec;
            getSourceAndDestinationLocationsInVector(sStr , dStr);
            bfsQueue    = MyQueue();
            bfsQueue.queuePush(&graphVector[sourceLoc]);
            graphVector[sourceLoc].isVisited = true;
        }
        
        int findCostForReachingDestination()
        {
            node_struct *parent = &graphVector[sourceLoc];
            cout<<parent->name<<endl;
            while (bfsQueue.checkIfQueueIsNotEmpty())
            {
                node_struct *poppedNode = &((queue_struct *)bfsQueue.queuePop())->data;
                for(int i = 0 ; i < poppedNode->children ; i++)
                {
                    if(!poppedNode->isVisited)
                    {
                        bfsQueue.queuePush(poppedNode->childrenNodes[i]);
                        graphVector[poppedNode->childrenNodes[i]->positionInList].isVisited = true;
                    }
                    if(poppedNode->cost + poppedNode->childCost[i] < graphVector[poppedNode->childrenNodes[i]->positionInList].cost)
                    {
                        graphVector[poppedNode->childrenNodes[i]->positionInList].cost = poppedNode->cost + poppedNode->childCost[i];
                    }
                }
            }
            return graphVector[destinationLoc].cost;
        }
};

int main(int argc, const char * argv[])
{
    vector<string> dictionaryVector = {"BCCI" , "AICC" , "ICC" , "CCI" , "MCC" , "MCA" , "ACC"};
    Graph graph                     = Graph(dictionaryVector);
    vector<node_struct> graphVector = graph.createNodesVector();
    //graph.display(graphVector);
    Engine e                        = Engine(graphVector , "AICC" , "ACC");
    cout<<e.findCostForReachingDestination()<<endl;
    return 0;
}
